FROM deepfabric/elasticell-dev

RUN mkdir -p /apps/deepfabric

COPY ./ /go/src/gitee.com/wanttobeamaster/elasticell

RUN cd /go/src/gitee.com/wanttobeamaster/elasticell \
    && go test -v ./...

WORKDIR /apps/deepfabric